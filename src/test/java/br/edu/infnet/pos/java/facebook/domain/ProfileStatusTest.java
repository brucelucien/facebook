package br.edu.infnet.pos.java.facebook.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProfileStatusTest {

	@Test
	public void deveTerAQuantidadeCertaDeItens() {
		assertEquals(2, ProfileStatus.values().length);
	}

	@Test
	public void deveTerOsItensCorretos() {
		ProfileStatus.valueOf("ACTIVE");
		ProfileStatus.valueOf("INACTIVE");
	}

}
