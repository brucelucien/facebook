package br.edu.infnet.pos.java.facebook.domain;

import static org.junit.Assert.*;

import java.time.LocalDate;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class BasicInformationTest {

	@InjectMocks
	public BasicInformation basicInformation;

	@Test
	public void deveTerSex() {
		basicInformation.setSex(BasicInformationSex.FEMALE);
		assertEquals(BasicInformationSex.FEMALE, basicInformation.getSex());
	}

	@Test
	public void deveTerInterestedInMen() {
		basicInformation.setInterestedInMen(true);
		assertTrue(basicInformation.getInterestedInMen());
	}

	@Test
	public void deveTerInterestedInWoman() {
		basicInformation.setInterestedInWoman(true);
		assertTrue(basicInformation.getInterestedInWoman());
	}

	@Test
	public void deveTerLookingForFriendship() {
		basicInformation.setLookingForFriendship(true);
		assertTrue(basicInformation.getLookingForFriendship());
	}

	@Test
	public void deveTerLookingForARelationship() {
		basicInformation.setLookingForARelationship(true);
		assertTrue(basicInformation.getLookingForARelationship());
	}

	@Test
	public void deveTerLookingForWhateverICanGet() {
		basicInformation.setLookingForWhateverICanGet(true);
		assertTrue(basicInformation.getLookingForWhateverICanGet());
	}

	@Test
	public void deveTerLookingForDating() {
		basicInformation.setLookingForDating(true);
		assertTrue(basicInformation.getLookingForDating());
	}

	@Test
	public void deveTerBirthdayDay() {
		LocalDate localDate = LocalDate.now();
		basicInformation.setBirthdayDay(localDate);
		assertEquals(localDate, basicInformation.getBirthdayDay());
	}

	@Test
	public void deveTerHometown() {
		final String hometown = "Porto Alegre paralelo 30";
		basicInformation.setHometown(hometown);
		assertEquals(hometown, basicInformation.getHometown());
	}

	@Test
	public void deveTerPoliticalViews() {
		basicInformation.setPoliticalView(BasicInformationPoliticalView.INDIFFERENT);
		assertEquals(BasicInformationPoliticalView.INDIFFERENT, basicInformation.getPoliticalView());
	}

	@Test
	public void deveTerReligiousViews() {
		final String religiousView = "Fé em Tupã";
		basicInformation.setReligiousView(religiousView);
		assertEquals(religiousView, basicInformation.getReligiousView());
	}

	@Test
	public void valoresDeInicializacaoDevemSerCorretos() {
		assertEquals(BasicInformationSex.UNINFORMED, basicInformation.getSex());
		assertFalse(basicInformation.getInterestedInMen());
		assertFalse(basicInformation.getInterestedInWoman());
		assertFalse(basicInformation.getLookingForFriendship());
		assertFalse(basicInformation.getLookingForARelationship());
		assertFalse(basicInformation.getLookingForWhateverICanGet());
		assertFalse(basicInformation.getLookingForDating());
		assertEquals(LocalDate.MIN, basicInformation.getBirthdayDay());
		assertEquals("", basicInformation.getHometown());
		assertEquals(BasicInformationPoliticalView.UNINFORMED, basicInformation.getPoliticalView());
		assertEquals("", basicInformation.getReligiousView());
	}

}
