package br.edu.infnet.pos.java.facebook.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class BasicInformationSexTest {

	@Test
	public void deveTerAQtdCertaDeItens() {
		assertEquals(3, BasicInformationSex.values().length);
	}

	@Test
	public void deveTerOsItensCorretos() {
		BasicInformationSex.valueOf("MALE");
		BasicInformationSex.valueOf("FEMALE");
		BasicInformationSex.valueOf("UNINFORMED");
	}

}
