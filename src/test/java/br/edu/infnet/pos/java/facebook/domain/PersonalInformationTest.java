package br.edu.infnet.pos.java.facebook.domain;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class PersonalInformationTest {

	@InjectMocks
	private PersonalInformation personalInformation;

	@Test
	public void deveTerActivites() {
		final String activities = "Academia Todo Dia de Maresia";
		personalInformation.setActivities(activities);
		assertEquals(activities, personalInformation.getActivities());
	}

	@Test
	public void deveTerInterests() {
		final String interests = "Poker às quartas e Futebol aos sábados";
		personalInformation.setInterests(interests);
		assertEquals(interests, personalInformation.getInterests());
	}

	@Test
	public void deveTerFavoriteMusic() {
		final String favoriteMusic = "Tá Vendo Aquela Lua (Exaltassamba)";
		personalInformation.setFavoriteMusic(favoriteMusic);
		assertEquals(favoriteMusic, personalInformation.getFavoriteMusic());
	}

	@Test
	public void deveTerFavoriteTVShows() {
		final String favoriteTVShows = "Mestres Churrasqueiros (BBQ Pitmasters, no TLC)";
		personalInformation.setFavoriteTVShows(favoriteTVShows);
		assertEquals(favoriteTVShows, personalInformation.getFavoriteTVShows());
	}

	@Test
	public void deveTerFavoriteMovies() {
		final String favoriteMovies = "Star Wars, Ender's Game, Inception, Interestelar";
		personalInformation.setFavoriteMovies(favoriteMovies);
		assertEquals(favoriteMovies, personalInformation.getFavoriteMovies());
	}

	@Test
	public void deveTerFavoriteBooks() {
		final String favoriteBooks = "Duna, 1984, Admirável Mundo Novo e As Crônicas do Gelo e do Fogo";
		personalInformation.setFavoriteBooks(favoriteBooks);
		assertEquals(favoriteBooks, personalInformation.getFavoriteBooks());
	}

	@Test
	public void deveTerFavoriteQuotes() {
		final String favoriteQuotes = "Quanto mais se olha, mais se vê. (PIRSIG, Robert M.)";
		personalInformation.setFavoriteQuotes(favoriteQuotes);
		assertEquals(favoriteQuotes, personalInformation.getFavoriteQuotes());
	}

	@Test
	public void deveTerAboutMe() {
		final String aboutMe = "Eu sou apenas um rapaz latino americano, sem dinheiro no banco e vindo do interior.";
		personalInformation.setAboutMe(aboutMe);
		assertEquals(aboutMe, personalInformation.getAboutMe());
	}

	@Test
	public void deveInicializarCorretamenteOsCampos() {
		assertEquals("", personalInformation.getActivities());
		assertEquals("", personalInformation.getInterests());
		assertEquals("", personalInformation.getFavoriteMusic());
		assertEquals("", personalInformation.getFavoriteTVShows());
		assertEquals("", personalInformation.getFavoriteMovies());
		assertEquals("", personalInformation.getFavoriteBooks());
		assertEquals("", personalInformation.getFavoriteQuotes());
		assertEquals("", personalInformation.getAboutMe());
	}

}
