package br.edu.infnet.pos.java.facebook.domain;

import static org.junit.Assert.*;

import java.io.Serializable;

import org.junit.Before;
import org.junit.Test;

public class EntityFBTest {
	
	private EntityFB entidade;
	
	@Before
	public void inicializarTeste() {
		entidade = new EntityFB() {
		};
	}

	@Test
	public void deveTerId() {
		final Integer ID_ENTIDADE = 13421;
		entidade.setId(ID_ENTIDADE);
		assertEquals(ID_ENTIDADE, entidade.getId());
	}
	
	@Test
	public void idDeveSerInicializadoComMenosUm() {
		assertEquals(-1, entidade.getId().intValue());
	}
	
	@Test
	public void entityDeveImplementarSerializable() {
		assertTrue(entidade instanceof Serializable);
	}

}
