package br.edu.infnet.pos.java.facebook.domain;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class AccountTest {

	@InjectMocks
	private Account account;

	@Test
	public void deveTerName() {
		final String NAME = "Nome da Conta";
		account.setName(NAME);
		assertEquals(NAME, account.getName());
	}

	@Test
	public void deveTerEMail() {
		final String ADDRESS = "brucelucien@gmail.com";
		final EMail email = new EMail();
		email.setAddress(ADDRESS);
		account.setEMail(email);
		assertEquals(ADDRESS, account.getEMail().getAddress());
	}

	@Test
	public void deveTerPassword() {
		final String PASSWORD = "s3nh4";
		account.setPassword(PASSWORD);
		assertEquals(PASSWORD, account.getPassword());
	}

	@Test
	public void deveTerSecurityQuestion() {
		final String SECURITY_QUESTION = "Qual é o nome do meu cachorro?";
		account.setSecurityQuestion(SECURITY_QUESTION);
		assertEquals(SECURITY_QUESTION, account.getSecurityQuestion());
	}

	@Test
	public void deveTerSecurityAnswer() {
		final String SECURITY_ANSWER = "Júnior";
		account.setSecurityAnswer(SECURITY_ANSWER);
		assertEquals(SECURITY_ANSWER, account.getSecurityAnswer());
	}

	@Test
	public void deveTerFriends() {
		final String NOME_B = "Nome B";
		final Account friendA = new Account();
		final Account friendB = new Account();
		friendB.setName(NOME_B);
		final List<Account> friends = new ArrayList<>();
		friends.add(friendA);
		friends.add(friendB);
		account.setFriends(friends);
		assertEquals(2, account.getFriends().size());
		assertEquals(NOME_B, account.getFriends().get(1).getName());
	}

	@Test
	public void deveTerProfile() {
		final Profile profile = new Profile();
		profile.setStatus(ProfileStatus.ACTIVE);
		account.setProfile(profile);
		assertEquals(ProfileStatus.ACTIVE, account.getProfile().getStatus());
	}

	@Test
	public void valoresDefaultDevemSerOsCorretos() {
		assertEquals("", account.getName());
		assertNotNull(account.getEMail());
		assertEquals("", account.getPassword());
		assertEquals("", account.getSecurityQuestion());
		assertEquals("", account.getSecurityAnswer());
		assertNotNull(account.getFriends());
		assertNotNull(account.getProfile());
	}

}
