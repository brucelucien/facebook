package br.edu.infnet.pos.java.facebook.domain;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ProfileTest {

	@InjectMocks
	private Profile profile;

	@Test
	public void deveTerStatus() {
		profile.setStatus(ProfileStatus.INACTIVE);
		assertEquals(ProfileStatus.INACTIVE, profile.getStatus());
	}

	@Test
	public void deveSerPossivelAlterarStatus() {
		profile.setStatus(ProfileStatus.ACTIVE);
		assertEquals(ProfileStatus.ACTIVE, profile.getStatus());
	}

	@Test
	public void deveTerBasicInformation() {
		final BasicInformation basicInformation = new BasicInformation();
		basicInformation.setReligiousView("Thor filho de Odin");
		profile.setBasicInformation(basicInformation);
		assertEquals(basicInformation.getReligiousView(), profile.getBasicInformation().getReligiousView());
	}

	@Test
	public void deveTerPersonalInformation() {
		final PersonalInformation personalInformation = new PersonalInformation();
		personalInformation.setFavoriteBooks("Sapiens (HARARI, Yuval Noah)");
		profile.setPersonalInformation(personalInformation);
		assertEquals(personalInformation.getFavoriteBooks(), profile.getPersonalInformation().getFavoriteBooks());
	}

	@Test
	public void camposAoInicializarDeveTerValoresCorretos() {
		assertEquals(ProfileStatus.INACTIVE, profile.getStatus());
		assertNotNull(profile.getBasicInformation());
		assertNotNull(profile.getPersonalInformation());
	}

}
