package br.edu.infnet.pos.java.facebook.domain;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class EMailTest {

	@InjectMocks
	private EMail email;

	@Test
	public void deveTerAddress() {
		final String ADDRESS = "brucelucien@gmail.com";
		email.setAddress(ADDRESS);
		assertEquals(ADDRESS, email.getAddress());
	}

	@Test
	public void addressDeveSerVazioPorDefault() {
		assertEquals("", email.getAddress());
	}

}
