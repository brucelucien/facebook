package br.edu.infnet.pos.java.facebook.domain;

import static org.junit.Assert.*;

import org.junit.Test;

public class BasicInformationPoliticalViewTest {

	@Test
	public void deveTerAQtdCertaDeItens() {
		assertEquals(19, BasicInformationPoliticalView.values().length);
	}

	@Test
	public void deveTerOsItensCorretos() {
		BasicInformationPoliticalView.valueOf("DEMOCRATIC");
		BasicInformationPoliticalView.valueOf("DEMOCRATIC_SOCIALISM");
		BasicInformationPoliticalView.valueOf("BLUE_DOG_DEMOCRAT");
		BasicInformationPoliticalView.valueOf("CONSERVATIVE");
		BasicInformationPoliticalView.valueOf("CONSERVATIVE_LIBERTARIAN");
		BasicInformationPoliticalView.valueOf("MODERATE_CONSERVATIVE");
		BasicInformationPoliticalView.valueOf("VERY_CONSERVATIVE");
		BasicInformationPoliticalView.valueOf("LIBERAL");
		BasicInformationPoliticalView.valueOf("LIBERTARIAN");
		BasicInformationPoliticalView.valueOf("MODERATE_LIBERAL");
		BasicInformationPoliticalView.valueOf("VERY_LIBERAL");
		BasicInformationPoliticalView.valueOf("REPUBLICAN");
		BasicInformationPoliticalView.valueOf("SOCIALIST");
		BasicInformationPoliticalView.valueOf("SOCIALIST_LIBERTARIAN");
		BasicInformationPoliticalView.valueOf("GREEN_PARTY");
		BasicInformationPoliticalView.valueOf("INDEPENDENT");
		BasicInformationPoliticalView.valueOf("INDIFFERENT");
		BasicInformationPoliticalView.valueOf("OTHER");
		BasicInformationPoliticalView.valueOf("UNINFORMED");
	}
}
