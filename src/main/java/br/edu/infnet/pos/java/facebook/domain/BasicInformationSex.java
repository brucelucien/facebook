package br.edu.infnet.pos.java.facebook.domain;

public enum BasicInformationSex {

	MALE, FEMALE, UNINFORMED;

}
