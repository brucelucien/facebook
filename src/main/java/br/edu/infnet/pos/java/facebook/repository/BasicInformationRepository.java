package br.edu.infnet.pos.java.facebook.repository;

import org.springframework.data.repository.CrudRepository;

import br.edu.infnet.pos.java.facebook.domain.BasicInformation;

public interface BasicInformationRepository extends CrudRepository<BasicInformation, Integer> {

}
