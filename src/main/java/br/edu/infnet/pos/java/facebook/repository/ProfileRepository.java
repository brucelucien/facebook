package br.edu.infnet.pos.java.facebook.repository;

import org.springframework.data.repository.CrudRepository;

import br.edu.infnet.pos.java.facebook.domain.Profile;

public interface ProfileRepository extends CrudRepository<Profile, Integer> {

}
