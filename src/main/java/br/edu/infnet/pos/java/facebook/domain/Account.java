package br.edu.infnet.pos.java.facebook.domain;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ACCOUNT")
public class Account extends EntityFB {

	private static final long serialVersionUID = 1496402801078692531L;

	@Column(name = "NAME")
	private String name;

	@Column(name = "PASSWORD")
	private String password;

	@Column(name = "SECURITY_QUESTION")
	private String securityQuestion;

	@Column(name = "SECURITY_ANSWER")
	private String securityAnswer;

	@Embedded
	private EMail email;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PROFILE_ID")
	private Profile profile;

	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "FRIENDSHIP", joinColumns = { @JoinColumn(name = "ACCOUNT_ID") }, inverseJoinColumns = {
			@JoinColumn(name = "ID_ACCOUNT_FRIEND") })
	private List<Account> friends;

	public Account() {
		name = "";
		email = new EMail();
		password = "";
		securityQuestion = "";
		securityAnswer = "";
		friends = new ArrayList<>();
		profile = new Profile();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void setEMail(EMail email) {
		this.email = email;
	}

	public EMail getEMail() {
		return email;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPassword() {
		return password;
	}

	public void setSecurityQuestion(String securityQuestion) {
		this.securityQuestion = securityQuestion;
	}

	public Object getSecurityQuestion() {
		return securityQuestion;
	}

	public void setSecurityAnswer(String securityAnswer) {
		this.securityAnswer = securityAnswer;
	}

	public Object getSecurityAnswer() {
		return securityAnswer;
	}

	public void setFriends(List<Account> friends) {
		this.friends = friends;
	}

	public List<Account> getFriends() {
		return friends;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}

}
