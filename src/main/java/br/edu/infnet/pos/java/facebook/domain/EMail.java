package br.edu.infnet.pos.java.facebook.domain;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class EMail implements Serializable {

	private static final long serialVersionUID = 7613126262003148556L;

	@Column(name = "EMAIL_ADDRESS")
	private String address;

	public EMail() {
		address = "";
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Object getAddress() {
		return address;
	}

}
