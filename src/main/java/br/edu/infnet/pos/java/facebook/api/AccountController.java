package br.edu.infnet.pos.java.facebook.api;

import javax.inject.Inject;

import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import br.edu.infnet.pos.java.facebook.domain.Account;
import br.edu.infnet.pos.java.facebook.repository.AccountRepository;

@RestController
public class AccountController {

	@Inject
	private AccountRepository accountRepository;

	@RequestMapping(value = "/accounts", method = RequestMethod.POST)
	public ResponseEntity<?> create(@RequestBody Account account) {
		accountRepository.save(account);
		return new ResponseEntity<>(HttpStatus.CREATED);
	}

	@RequestMapping(value = "/accounts/{accountId}", method = RequestMethod.GET)
	public ResponseEntity<?> findById(@PathVariable String accountId) {
		Account account = accountRepository.findOne(Integer.valueOf(accountId));
		return new ResponseEntity<>(account, HttpStatus.OK);
	}

	@RequestMapping(value = "/accounts", method = RequestMethod.GET)
	public Page<?> findAll(Pageable pageable) {
		Page<Account> accounts = accountRepository.findAll(pageable);
		return accounts;
	}

	@RequestMapping(value = "/accounts/{accountId}", method = RequestMethod.DELETE)
	public ResponseEntity<?> delete(@PathVariable String accountId) {
		ResponseEntity<?> response = null;
		try {
			accountRepository.delete(Integer.valueOf(accountId));
			response = new ResponseEntity<>(HttpStatus.OK);
		} catch (EmptyResultDataAccessException e) {
			response = new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} catch (NumberFormatException e) {
			response = new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}
		return response;
	}

}
