package br.edu.infnet.pos.java.facebook.repository;

import org.springframework.data.repository.CrudRepository;

import br.edu.infnet.pos.java.facebook.domain.PersonalInformation;

public interface PersonalInformationRepository extends CrudRepository<PersonalInformation, Integer> {

}
