package br.edu.infnet.pos.java.facebook.domain;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PROFILE")
public class Profile extends EntityFB {

	private static final long serialVersionUID = -5240181769468149236L;

	@Column(name = "STATUS")
	@Enumerated(EnumType.STRING)
	private ProfileStatus status;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "BASIC_INFORMATION_ID")
	private BasicInformation basicInformation;

	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "PERSONAL_INFORMATION_ID")
	private PersonalInformation personalInformation;

	public Profile() {
		status = ProfileStatus.INACTIVE;
		basicInformation = new BasicInformation();
		personalInformation = new PersonalInformation();
	}

	public ProfileStatus getStatus() {
		return status;
	}

	public void setStatus(ProfileStatus status) {
		this.status = status;
	}

	public BasicInformation getBasicInformation() {
		return basicInformation;
	}

	public void setBasicInformation(BasicInformation basicInformation) {
		this.basicInformation = basicInformation;
	}

	public PersonalInformation getPersonalInformation() {
		return personalInformation;
	}

	public void setPersonalInformation(PersonalInformation personalInformation) {
		this.personalInformation = personalInformation;
	}

}
