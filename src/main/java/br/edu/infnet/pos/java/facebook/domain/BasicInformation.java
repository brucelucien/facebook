package br.edu.infnet.pos.java.facebook.domain;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
@Table(name = "BASIC_INFORMATION")
public class BasicInformation extends EntityFB {

	private static final long serialVersionUID = 1120048143961645305L;

	@Column(name = "SEX")
	@Enumerated(EnumType.STRING)
	private BasicInformationSex sex;

	@Column(name = "INTERESTED_IN_MEN")
	private Boolean interestedInMen;

	@Column(name = "INTERESTED_IN_WOMAN")
	private Boolean interestedInWoman;

	@Column(name = "LOOKING_FOR_FRIENDSHIP")
	private Boolean lookingForFriendship;

	@Column(name = "LOOKING_FOR_A_RELATIONSHIP")
	private Boolean lookingForARelationship;

	@Column(name = "LOOKING_FOR_WHATEVER_I_CAN_GET")
	private Boolean lookingForWhateverICanGet;

	@Column(name = "LOOKING_FOR_DATING")
	private Boolean lookingForDating;

	@Column(name = "BIRTHDAY_DAY")
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private LocalDate birthdayDay;

	@Column(name = "HOMETOWN")
	private String hometown;

	@Column(name = "POLITICAL_VIEW")
	@Enumerated(EnumType.STRING)
	private BasicInformationPoliticalView politicalView;

	@Column(name = "RELIGIOUS_VIEW")
	private String religiousView;

	public BasicInformation() {
		sex = BasicInformationSex.UNINFORMED;
		interestedInMen = false;
		interestedInWoman = false;
		lookingForFriendship = false;
		lookingForARelationship = false;
		lookingForWhateverICanGet = false;
		lookingForDating = false;
		birthdayDay = LocalDate.MIN;
		hometown = "";
		politicalView = BasicInformationPoliticalView.UNINFORMED;
		religiousView = "";
	}

	public BasicInformationSex getSex() {
		return sex;
	}

	public void setSex(BasicInformationSex sex) {
		this.sex = sex;
	}

	public Boolean getInterestedInMen() {
		return interestedInMen;
	}

	public void setInterestedInMen(Boolean interestedInMen) {
		this.interestedInMen = interestedInMen;
	}

	public Boolean getInterestedInWoman() {
		return interestedInWoman;
	}

	public void setInterestedInWoman(Boolean interestedInWoman) {
		this.interestedInWoman = interestedInWoman;
	}

	public Boolean getLookingForFriendship() {
		return lookingForFriendship;
	}

	public void setLookingForFriendship(Boolean lookingForFriendship) {
		this.lookingForFriendship = lookingForFriendship;
	}

	public Boolean getLookingForARelationship() {
		return lookingForARelationship;
	}

	public void setLookingForARelationship(Boolean lookingForARelationship) {
		this.lookingForARelationship = lookingForARelationship;
	}

	public Boolean getLookingForWhateverICanGet() {
		return lookingForWhateverICanGet;
	}

	public void setLookingForWhateverICanGet(Boolean lookingForWhateverICanGet) {
		this.lookingForWhateverICanGet = lookingForWhateverICanGet;
	}

	public Boolean getLookingForDating() {
		return lookingForDating;
	}

	public void setLookingForDating(Boolean lookingForDating) {
		this.lookingForDating = lookingForDating;
	}

	public LocalDate getBirthdayDay() {
		return birthdayDay;
	}

	public void setBirthdayDay(LocalDate birthdayDay) {
		this.birthdayDay = birthdayDay;
	}

	public String getHometown() {
		return hometown;
	}

	public void setHometown(String hometown) {
		this.hometown = hometown;
	}

	public BasicInformationPoliticalView getPoliticalView() {
		return politicalView;
	}

	public void setPoliticalView(BasicInformationPoliticalView politicalView) {
		this.politicalView = politicalView;
	}

	public String getReligiousView() {
		return religiousView;
	}

	public void setReligiousView(String religiousView) {
		this.religiousView = religiousView;
	}

}
