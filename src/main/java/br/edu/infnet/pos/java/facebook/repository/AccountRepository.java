package br.edu.infnet.pos.java.facebook.repository;

import org.springframework.data.repository.PagingAndSortingRepository;

import br.edu.infnet.pos.java.facebook.domain.Account;

public interface AccountRepository extends PagingAndSortingRepository<Account, Integer> {

}
