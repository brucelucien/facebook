package br.edu.infnet.pos.java.facebook.domain;

public enum ProfileStatus {

	ACTIVE, INACTIVE;

}
