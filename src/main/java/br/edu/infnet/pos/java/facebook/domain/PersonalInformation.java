package br.edu.infnet.pos.java.facebook.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "PERSONAL_INFORMATION")
public class PersonalInformation extends EntityFB {

	private static final long serialVersionUID = -8057553244525411664L;

	@Column(name = "ACTIVITIES")
	private String activities;

	@Column(name = "INTERESTS")
	private String interests;

	@Column(name = "FAVORITE_MUSIC")
	private String favoriteMusic;

	@Column(name = "FAVORITE_TV_SHOWS")
	private String favoriteTVShows;

	@Column(name = "FAVORITE_MOVIES")
	private String favoriteMovies;

	@Column(name = "FAVORITE_BOOKS")
	private String favoriteBooks;

	@Column(name = "FAVORITE_QUOTES")
	private String favoriteQuotes;

	@Column(name = "ABOUT_ME")
	private String aboutMe;

	public PersonalInformation() {
		activities = "";
		interests = "";
		favoriteMusic = "";
		favoriteTVShows = "";
		favoriteMovies = "";
		favoriteBooks = "";
		favoriteQuotes = "";
		aboutMe = "";
	}

	public String getActivities() {
		return activities;
	}

	public void setActivities(String activities) {
		this.activities = activities;
	}

	public String getInterests() {
		return interests;
	}

	public void setInterests(String interests) {
		this.interests = interests;
	}

	public String getFavoriteMusic() {
		return favoriteMusic;
	}

	public void setFavoriteMusic(String favoriteMusic) {
		this.favoriteMusic = favoriteMusic;
	}

	public String getFavoriteTVShows() {
		return favoriteTVShows;
	}

	public void setFavoriteTVShows(String favoriteTVShows) {
		this.favoriteTVShows = favoriteTVShows;
	}

	public String getFavoriteMovies() {
		return favoriteMovies;
	}

	public void setFavoriteMovies(String favoriteMovies) {
		this.favoriteMovies = favoriteMovies;
	}

	public String getFavoriteBooks() {
		return favoriteBooks;
	}

	public void setFavoriteBooks(String favoriteBooks) {
		this.favoriteBooks = favoriteBooks;
	}

	public String getFavoriteQuotes() {
		return favoriteQuotes;
	}

	public void setFavoriteQuotes(String favoriteQuotes) {
		this.favoriteQuotes = favoriteQuotes;
	}

	public String getAboutMe() {
		return aboutMe;
	}

	public void setAboutMe(String aboutMe) {
		this.aboutMe = aboutMe;
	}

}
